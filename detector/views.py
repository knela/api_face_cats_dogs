from django.http import HttpResponse, HttpResponseRedirect
from models import Image
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.core import serializers
from django.shortcuts import render
from django.urls import reverse

import numpy as np

import urllib 
import h5py # to save/load data files

import sys
import os

from scipy import misc

caffe_root = os.path.join('/opt', 'caffe/')  # this file should be run from {caffe_root}/examples (otherwise change this line)
sys.path.insert(0, os.path.join(caffe_root, 'python'))
os.environ['GLOG_minloglevel'] = '2' #Remove all logs, except for warnings and errors
import caffe


caffe.set_mode_cpu()

model_def = os.path.join(caffe_root, 'models', 'bvlc_reference_caffenet','deploy.prototxt')
model_weights = os.path.join(caffe_root, 'models','bvlc_reference_caffenet','bvlc_reference_caffenet.caffemodel')

net = caffe.Net(model_def,      # defines the structure of the model
                model_weights,  # contains the trained weights
                caffe.TEST)     # use test mode (e.g., don't perform dropout)


mu = np.load(os.path.join(caffe_root, 'python','caffe','imagenet','ilsvrc_2012_mean.npy'))
mu = mu.mean(1).mean(1)  # average over pixels to obtain the mean (BGR) pixel values

transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})

transformer.set_transpose('data', (2,0,1))  # move image channels to outermost dimension
transformer.set_mean('data', mu)            # subtract the dataset-mean value in each channel
transformer.set_raw_scale('data', 255)      # rescale from [0, 1] to [0, 255]
transformer.set_channel_swap('data', (2,1,0))  # swap channels from RGB to BGR

images_path = 'static/images/' #'VOCdevkit/VOC2007/JPEGImages/'
vectors_filename = os.path.join(images_path, 'vectors.h5')

def load_dataset(images_path):
    if os.path.exists(vectors_filename):
        with h5py.File(vectors_filename, 'r') as f:
            vectors = f['vectors'][()]
            img_files = f['img_files'][()]
    else:
        img_files = [f for f in os.listdir(images_path) if (('jpg' in f) or ('JPG') in f)] # Reads only jpeg images from images_path

        net_data_shape = net.blobs['data'].data.shape
        train_images = np.zeros(([len(img_files)] + list(net_data_shape[1:])))

        for (img_file, index) in zip(img_files, range(len(img_files))):
            image = caffe.io.load_image(os.path.join(images_path, img_file))
            train_images[index] = transformer.preprocess('data', image)

        vectors = np.zeros((train_images.shape[0], 1000))
        for n in range(0, train_images.shape[0], 10): # For each batch of 10 images:
            # This block can/should be parallelised!
            last_n = np.min((n+10, train_images.shape[0]))

            net.blobs['data'].data[0:last_n-n] = train_images[n:last_n]

            net.forward()

            vectors[n:last_n] = net.blobs['prob'].data[0:last_n-n]
        
        with h5py.File(vectors_filename, 'w') as f:
            f.create_dataset('vectors', data = vectors)
            f.create_dataset('img_files', data = img_files)
    return vectors, img_files



def predict_imageNet(image_filename):
    image = caffe.io.load_image(image_filename)
    net.blobs['data'].data[...] = transformer.preprocess('data', image)
    net.forward()
    return net.blobs['prob'].data[0] #Probabilities


class NearestNeighbours:
    def __init__(self, neighbours=10, Xtr=[], img_files=[]):
        self.neighbours = neighbours
        self.Xtr = Xtr
        self.img_files = img_files

    def predict(self, x):
        """ x is a test (query) sample vector of 1 x D dimensions """
        distances = np.sum(np.abs(self.Xtr-x), axis = 1)
        return np.argsort(distances) # returns an array of indices of of the samples, sorted by how similar they are to x.

    def retrieve(self, x):
        nearest_neighbours = self.predict(x)[:self.neighbours]
        names = [self.img_files[idx] for idx in nearest_neighbours]
        return names

def new_image_name(last_image_name = '001.jpg'):
    number = int(last_image_name[0:-4])
    number = number + 1
    new_name = "00{}.jpg".format(number)
    return new_name

class ImageView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ImageView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        return render(request, 'detector/search.html')

    def post(self, request):
        vectors, img_files = load_dataset(images_path)
        KNN = NearestNeighbours(
                Xtr = vectors, 
                img_files = img_files,
            )

        my_image_url = request.POST['url']

        if('jpg' in my_image_url) or ('png' in my_image_url):
            try:
                img = Image.objects.get(
                    url = my_image_url
                )
                probs = predict_imageNet(images_path + img.name)
            except Image.DoesNotExist:
                my_image_name = new_image_name(max(img_files))
                while os.path.exists(images_path + my_image_name):
                    my_image_name = new_image_name(my_image_name)

                my_image = images_path + my_image_name

                urllib.urlretrieve(my_image_url, my_image)
                img = Image.objects.create(
                    url = my_image_url,
                    name = my_image_name
                )

                probs = predict_imageNet(my_image)
                img_files = np.append(img_files, my_image_name)
                vectors = np.vstack((vectors, probs))
                with h5py.File(vectors_filename, 'w') as f:
                    f.create_dataset('vectors', data = vectors)
                    f.create_dataset('img_files', data = tuple(img_files))
            del vectors

            names = KNN.retrieve(probs)

            del probs
            data = Image.objects.filter(name__in = names).values()
            urls = []
            for d in data:
                urls.append(d['url'])
            names_and_urls = zip(names, urls)

            #return HttpResponseRedirect(data, content_type = 'application/json')
            return render(request, 'detector/search.html', locals())
        else:
            return render(request, 'detector/search.html',
                {'error_message': 'URL not found. Type a valid image url'})
                    # return HttpResponseRedirect(request, )
