from __future__ import unicode_literals

from django.db import models

class Image(models.Model):
    url = models.CharField(max_length = 500, primary_key = True)
    name = models.CharField(max_length = 50, blank = False)

    def __str__(self):
        return "{}. Origin: {}".format(self.name, self.url)