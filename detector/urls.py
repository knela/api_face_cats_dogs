from django.conf.urls import url
from detector.views import ImageView

urlpatterns = [
    url(r'^image/$', ImageView.as_view(), name = 'classify')
]